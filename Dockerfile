FROM ruby:2.5.3-alpine3.8

WORKDIR /app

COPY . /app

RUN bundle install --clean

EXPOSE 4567

CMD ["ruby", "app.rb"]

